package org.rotary.iyfr.plasticfreewaters.entity;

/**
 * Singleton Klasse: Speichert flüchtige 'States', die während einer Session benötigt werden,
 * jedoch nicht über das Beenden der App hinaus.
 */
public class States {
    /* SINGLETON */
    private static org.rotary.iyfr.plasticfreewaters.entity.States states;

    private boolean isTest = false;

    /* Klasse ist Singleton, daher privater Konstruktor. */
    private States() {/*NIX*/ }

    private static synchronized States getInstance() {
        if (states == null) {
            States.states = new States();
        }
        return states;
    }

    public static boolean isTest() {
        return getInstance().isTest;
    }

    public static void setTest(boolean trueFalse) {
        getInstance().isTest = trueFalse;
    }
}
