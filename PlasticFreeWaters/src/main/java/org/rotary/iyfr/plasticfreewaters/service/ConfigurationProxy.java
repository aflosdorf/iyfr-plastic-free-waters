package org.rotary.iyfr.plasticfreewaters.service;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.Spanned;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.Locale;

public class ConfigurationProxy {

    public static final int REQUESTCODE_LOCATION_PERMISSIONS = 1000;
    public static final int REQUESTCODE_CHOOSE_ACCOUNT = 1001;
    private static final String FIREBASE_KONFIG_WEBSITE_URI = "website_uri";
    private static final String FIREBASE_KONFIG_AUTHORITY_EMAIL_LIST = "report_email_address"; /* Production für it : 'sez3.uff4.rep3@mit.gov.it' */
    private static final String TEST_KONFIG_AUTHORITY_EMAIL_LIST = "iyfrplasticfreewaters@gmail.com";
    private static final String FIREBASE_KONFIG_SAVE_REPORTS_TO_STORAGE = "save_reports_to_storage";
    public static InputFilter symbolFilter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            StringBuilder filteredTextInput = new StringBuilder();
            for (int i = start; i < end; i++) {
                char currentChar = source.charAt(i);
                if (Character.isLetterOrDigit(currentChar)
                        || Character.isSpaceChar(currentChar)
                        || String.valueOf(currentChar).contains("?")
                        || String.valueOf(currentChar).contains("!")
                        || String.valueOf(currentChar).contains(".")
                        || String.valueOf(currentChar).contains(",")
                        || String.valueOf(currentChar).contains(":")
                        || String.valueOf(currentChar).contains(";")
                        || String.valueOf(currentChar).contains("-")
                        || String.valueOf(currentChar).contains("_")
                        || String.valueOf(currentChar).contains("+")
                        || String.valueOf(currentChar).contains("*")
                        || String.valueOf(currentChar).contains("/")
                        || String.valueOf(currentChar).contains("%")
                        || String.valueOf(currentChar).contains("(")
                        || String.valueOf(currentChar).contains(")")
                        || String.valueOf(currentChar).contains("=")
                        || String.valueOf(currentChar).contains("#")
                        || String.valueOf(currentChar).contains("@")
                        || String.valueOf(currentChar).contains("€")
                        || String.valueOf(currentChar).contains("$")
                        || String.valueOf(currentChar).contains("[")
                        || String.valueOf(currentChar).contains("]")
                        || String.valueOf(currentChar).contains("\n")
                ) {
                    filteredTextInput.append(currentChar);
                }
            }
            return filteredTextInput.toString();
        }
    };

    public static String getAppVersion() {
        String version = "";
        try {
            PackageInfo packageInfo = App.context().getPackageManager().getPackageInfo(App.context().getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public static int getAppVersionCode() {
        try {
            PackageInfo packageInfo = App.context().getPackageManager().getPackageInfo(App.context().getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return Integer.MAX_VALUE;
    }

    public static String getAppVersionWithVersionCode() {
        return getAppVersion() + " (" + getAppVersionCode() + ")";
    }

    public static Locale getCurrentSystemLocale() {
        Configuration config = App.context().getResources().getConfiguration();
        Locale sysLocale;
        sysLocale = config.getLocales().get(0);
        return sysLocale;
    }

    /*
     * national marine authority
     * TODO: If the app ever needs to work in multiple countries and/or with multiple jurisdictions (coastguards), this method must be rewritten so that the correct address is selected from a configurable list.
     */
    public static String getEMailAdressOfAuthority() {
        FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        String authorityListAsJson = firebaseRemoteConfig.getString(FIREBASE_KONFIG_AUTHORITY_EMAIL_LIST);
        if (ApplicationMonitor.isTest()) return TEST_KONFIG_AUTHORITY_EMAIL_LIST;
        else return authorityListAsJson;
    }

    public static String getTestString() {
        FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        String testString = firebaseRemoteConfig.getString("test_string");
        return testString;
    }

    public static String getWebSiteUrl() {
        FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        String url = firebaseRemoteConfig.getString(FIREBASE_KONFIG_WEBSITE_URI);
        return url;
    }

    private static boolean isAccessCoarseLocation() {
        return ContextCompat.checkSelfPermission(App.context(), Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    private static boolean isAccessFineLocation() {
        return ContextCompat.checkSelfPermission(App.context(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isLocationAccessEnabled() {
        final LocationManager locationManager = (LocationManager) App.context()
                .getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return isGPSEnabled || isNetworkEnabled;
    }

    public static boolean isLocationPermissionEnabled() {
        boolean accessFineLocation = ConfigurationProxy.isAccessFineLocation();
        boolean accessCoarseLocation = false;
        if (!accessFineLocation)
            accessCoarseLocation = ConfigurationProxy.isAccessCoarseLocation();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return accessFineLocation || accessCoarseLocation;
        }
        return accessFineLocation;
    }

    public static boolean isSaveReportToStorage() {
        FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        Boolean isSaveReportToStorage = Boolean.valueOf(firebaseRemoteConfig.getString(FIREBASE_KONFIG_SAVE_REPORTS_TO_STORAGE));
        return isSaveReportToStorage;
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void requestLocationPermissions(Activity activity) {
        String[] permissions = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
        ActivityCompat.requestPermissions(activity, permissions, ConfigurationProxy.REQUESTCODE_LOCATION_PERMISSIONS);
    }

    public static void requestLocationSettings(Activity activity) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        activity.startActivity(intent);
    }

    boolean isValidEmailAddress(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}