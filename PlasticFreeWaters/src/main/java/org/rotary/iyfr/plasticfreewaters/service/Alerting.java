package org.rotary.iyfr.plasticfreewaters.service;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;

import org.rotary.iyfr.plasticfreewaters.service.App;

public class Alerting {

    public static final long[] VIBRATE_PATTERN_HAPTIC_FEEDBACK = new long[]{0, 50};

    public static void playVibrator() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Vibrator vibrator = (Vibrator) App.context().getSystemService(Context.VIBRATOR_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    VibrationEffect effect = VibrationEffect.createWaveform(VIBRATE_PATTERN_HAPTIC_FEEDBACK, -1);
                    vibrator.vibrate(effect);
                } else {
                    //deprecated in API 26
                    vibrator.vibrate(50);
                }
            }
        });
    }
}
