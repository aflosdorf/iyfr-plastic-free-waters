package org.rotary.iyfr.plasticfreewaters.service;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.entity.States;

public class ApplicationMonitor {

    private static final String APP_START = "iyfr_app_start";

    private Context context;
    private SharedPreferencesEditor mSharedPreferencesEditor;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String mAndroidId;
    private String mAppVersion;

    /**
     * Initialisiert u.a. FirebaseAnalytics und FirebaseRemoteConfig.
     **/
    public ApplicationMonitor() {
        context = App.context();
        mSharedPreferencesEditor = new SharedPreferencesEditor();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        boolean isAnalyticsCollectionEnabled = mSharedPreferencesEditor.isAnalyticsCollectionEnabled();
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(isAnalyticsCollectionEnabled);
        mAndroidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        mAppVersion = ConfigurationProxy.getAppVersionWithVersionCode();
        initRemoteConfiguration();
    }

    /**
     * Aktiviere Firebase Remote Config
     */
    public static void initRemoteConfiguration() {
        long cacheExpirationSeconds = 2 * 60 * 60; // 2 Stunden
        FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
        firebaseRemoteConfig.fetch(cacheExpirationSeconds);
        firebaseRemoteConfig.activate();
    }

    /**
     * ACHTUNG: Nicht im Konstruktor aufrufen, SecurityException bei Neuinstallation!
     * <p>
     * Wenn eine Datei mit Namen 'test.routes' im Files Ordner der App abgelegt ist oder
     * die entsprechende Metadata Variable im Manifest gesetzt ist, dann steuert dies das
     * Testverhalten der App wie folgt:
     * <p>
     * - die Anzahl aufgezeichneter Wegpunkte werden in Notification angezeigt,
     * - Firebase Event Logging wird disabled,
     * - Rating am Ende der Route wird nicht gespeichert,
     * - beim Remote Storage Speichern wird ein "TEST" Prefix gesetzt.
     * - u.v.m., siehe auch {@link States}.isTest() - Hier wird nach App Start der Status gespeichert
     *
     * @return true/false
     */
    public static boolean isTest() {
        /* Manifest Meta Data:
         * Siehe: https://stackoverflow.com/questions/40959903/exclude-testing-device-from-firebase-analytics-logging */
        boolean manifestMetadataIsTest;
        try {
            ApplicationInfo applicationInfo = App.context().getPackageManager().getApplicationInfo(App.context().getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;
            manifestMetadataIsTest = bundle.getBoolean("is_test");
        } catch (Exception e) {
            manifestMetadataIsTest = false;
        }
        /* Datei im Files Folder der App */
        boolean fileExistInFolder = LocalFileStorageService.doesFileExistInFolder(null, "test.iyfr");

        return manifestMetadataIsTest || fileExistInFolder;
    }

    public static void logFirebaseCrashlyticsEvent(Exception e) {
        if (!States.isTest())
            FirebaseCrashlytics.getInstance().recordException(e);
        else
            e.printStackTrace();
    }

    private void logFirebaseAnalyticsEvent(String event, Bundle bundle) {
        if (!States.isTest())
            mFirebaseAnalytics.logEvent(event, bundle);
    }

    public void logEventAppStart() {
        Bundle bundle = new Bundle();
        logFirebaseAnalyticsEvent(APP_START, bundle);
    }
}