package org.rotary.iyfr.plasticfreewaters.entity;

import android.location.Location;

import java.io.Serializable;

public class Waypoint implements Serializable {

    private double altitude;
    private long age;
    private double latitude;
    private double longitude;

    public Waypoint(Location location) {
        this.altitude = location.getAltitude();
        this.age = location.getTime();
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
    }

    public static String getLatLngFormattedNorthEast(double latitude, double longitude, String divider) {
        /* Source: https://stackoverflow.com/questions/38547870/android-how-to-convert-latitude-longitude-into-degree-format*/
        StringBuilder builder = new StringBuilder();
        if (latitude < 0) {
            builder.append("S ");
        } else {
            builder.append("N ");
        }

        String latitudeDegrees = Location.convert(Math.abs(latitude), Location.FORMAT_SECONDS);
        String[] latitudeSplit = latitudeDegrees.split(":");
        builder.append(latitudeSplit[0]);
        builder.append("°");
        builder.append(latitudeSplit[1]);
        builder.append("'");
        builder.append(latitudeSplit[2]);
        builder.append("\"");

        builder.append(divider);

        if (longitude < 0) {
            builder.append("W ");
        } else {
            builder.append("E ");
        }

        String longitudeDegrees = Location.convert(Math.abs(longitude), Location.FORMAT_SECONDS);
        String[] longitudeSplit = longitudeDegrees.split(":");
        builder.append(longitudeSplit[0]);
        builder.append("°");
        builder.append(longitudeSplit[1]);
        builder.append("'");
        builder.append(longitudeSplit[2]);
        builder.append("\"");

        return builder.toString();
    }

    public long getAge() {
        return age;
    }

    public double getAltitude() {
        return altitude;
    }

    public String getLatLngFormatted() {
        return getLatitude() + ", " + getLongitude();
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
