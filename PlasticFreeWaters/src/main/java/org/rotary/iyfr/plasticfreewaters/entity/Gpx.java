package org.rotary.iyfr.plasticfreewaters.entity;

import org.rotary.iyfr.plasticfreewaters.service.App;
import org.rotary.iyfr.plasticfreewaters.service.ConfigurationProxy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Gpx {
    private Waypoint waypoint;
    private long time = 0;
    private String name = "";
    private String type = "";
    private String description = "";
    private String author = "";

    public Gpx(String name, String type, String description, String author, Waypoint waypoint) {
        this.name = name;
        this.type = type;
        this.description = description;
        this.author = author;
        this.waypoint = waypoint;
        this.time = waypoint.getAge();
    }

    private String getCreator() {
        return "IYFR " + App.getAppVersionWithVersionCode() + " - " + ConfigurationProxy.getWebSiteUrl();
    }

    private String getDescription() {
        return description;
    }

    private String getFooter() {
        String footer = "</gpx>";
        return footer;
    }

    private String getHeader() {
        String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n" +
                "<gpx version=\"1.0\"" +
                "creator=\"" + getCreator() + "\" " +
                "name=\"" + getName() + "\" " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  " +
                "xmlns=\"http://www.topografix.com/GPX/1/0\" " +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd\">" + "\n";
        return header;
    }

    private String getName() {
        return name;
    }

    private long getTime() {
        return time;
    }

    private String getTimeFormatted(long time) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(new Date(time));
    }

    private String getWpt() {
        String wptHeader = "<wpt lat=\"" + waypoint.getLatitude() + "\" lon=\"" + waypoint.getLongitude() + "\">\n" +
                "\t<ele>" + waypoint.getAltitude() + "</ele>\n" +
                "\t<time>" + getTimeFormatted(waypoint.getAge()) + "</time>\n";
        String wptDescription = "\t<desc>" + getDescription() + "</desc>\n";
        String wptFooter = "</wpt>\n";
        /* It may me, that the user hasn't entered a description. If so, we leave the attribute 'description' out */
        if (getDescription() != null) {
            return wptHeader + wptDescription + wptFooter;
        } else {
            return wptHeader + wptFooter;
        }
    }

    public String createGpxContent() {
        return getHeader() + getWpt() + getFooter();
    }
}
