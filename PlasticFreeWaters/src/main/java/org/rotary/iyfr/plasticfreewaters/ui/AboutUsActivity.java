package org.rotary.iyfr.plasticfreewaters.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.service.ConfigurationProxy;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
    }

    public void onClickFabVisitWebsite(View view) {
        /* Open a browser to view website of Operation PFW. */
        Uri url = Uri.parse(ConfigurationProxy.getWebSiteUrl());
        Intent webSiteActivity = new Intent(Intent.ACTION_VIEW);
        webSiteActivity.setData(url);
        startActivity(webSiteActivity);
    }
}