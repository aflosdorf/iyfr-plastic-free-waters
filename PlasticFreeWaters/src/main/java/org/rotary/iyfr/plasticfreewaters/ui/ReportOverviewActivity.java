package org.rotary.iyfr.plasticfreewaters.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListView;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.service.AlertBuilder;
import org.rotary.iyfr.plasticfreewaters.service.Alerting;
import org.rotary.iyfr.plasticfreewaters.entity.Report;
import org.rotary.iyfr.plasticfreewaters.service.LocalFileStorageService;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReportOverviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_overview);
        ReportOverviewActivity activity = this;
        /* Find all saved reports */
        ArrayList<Report> reports = new ArrayList<>();
        List<String> listReportFolder = LocalFileStorageService.listReportFolder();
        if (listReportFolder != null && listReportFolder.size() > 0) {
            /* We need to sort the list, because not every vendor lists file entries sorted by default. */
            Collections.sort(listReportFolder);
            for (int i = listReportFolder.size() - 1; i >= 0; i--) {
                Report report = LocalFileStorageService.readReport(listReportFolder.get(i));
                if (report != null)
                    reports.add(report);
            }
        }
        boolean hasReports = reports.size() > 0;
        /* After we loaded all saved reports we have to reset the global report to ensure,
         * that next 'new report' a new report ist generated or the last working status is present. */
        Report.reset();
        /* Build card view with all found reports */
        MaterialListView cardListView = (MaterialListView) findViewById(R.id.cardListView);
        addWelcomeCard(cardListView, hasReports);
        if (hasReports) {
            for (Report report : reports) { /* We do have report(s) */
                addCard(report, cardListView);
            }
            cardListView.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(@NonNull Card card, int position) {
                    /* Open this report in ReportActivity; Ensure, that you read the saved report, otherwise
                     * it might be possible to display reverted information. */
                    Report report = (Report) card.getTag();
                    /* our welcome card doesn't have an undelying report */
                    if (report == null) return;
                    /* If we have a report, we start the detail view on it */
                    Intent newReportIntent = new Intent(activity, ReportActivity.class);
                    newReportIntent.putExtra(ReportActivity.INTENT_EXTRA_REPORT_ID, report.getId());
                    startActivity(newReportIntent);
                }

                @Override
                public void onItemLongClick(@NonNull Card card, int position) {
                    /* Ask if this report should be deleted */
                    Alerting.playVibrator();
                    AlertBuilder.showDeleteReport(card, activity);
                }
            });
            cardListView.scrollToPosition(0);
        }
    }

    private void addCard(Report report, MaterialListView cardListView) {
        Drawable drawable = null;
        if (report.hasPictures()) {
            String reportFolder = LocalFileStorageService.getReportFolderPath(report.getId());
            File picture = new File(reportFolder, String.valueOf(report.getPictures().get(0)));
            Bitmap bitmap = BitmapFactory.decodeFile(picture.getPath());
            drawable = new BitmapDrawable(bitmap);
        } else {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.logo_rotary_icon);
            drawable = new BitmapDrawable(bitmap);
        }

        String title = report.getNameForUi("\n");
        String description = report.getDescription();
        if (description == null || description.trim().isEmpty())
            description = getString(R.string.no_description_available);
        String dateTime = report.getLocalDateAndTime();
        Card card = new Card.Builder(this)
                .withProvider(new CardProvider())
                .setLayout(R.layout.material_basic_image_buttons_card_layout_custom) // Hint: This is a customised copy of 'material_basic_image_buttons_card_layout'
                .setTitle(title)
                .setTitleGravity(Gravity.START)
                .setSubtitle(dateTime)
                .setSubtitleGravity(Gravity.START)
                .setSubtitleColor(Color.GRAY)
                .setDescription(description)
                .setDescriptionGravity(Gravity.START)
                .setDrawable(drawable)
                .endConfig()
                .setTag(report)
                .build();
        cardListView.getAdapter().add(card);
    }

    private void addWelcomeCard(MaterialListView cardListView, boolean hasReports) {
        String title = getString(R.string.menu_item_reports);
        String description = getString(R.string.reports_description);
        if (!hasReports) description = getString(R.string.reports_welcome_description);
        Card card = new Card.Builder(this)
                .withProvider(new CardProvider())
                .setLayout(R.layout.material_basic_buttons_card)
                .setTitle(title)
                .setTitleColor(getResources().getColor(R.color.white, null))
                .setDescription(description)
                .setDescriptionColor(getResources().getColor(R.color.white, null))
                .setBackgroundColor(getResources().getColor(R.color.rotary_blue, null))
                .endConfig()
                .build();
        cardListView.getAdapter().add(card);
    }
}