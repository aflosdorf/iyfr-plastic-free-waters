package org.rotary.iyfr.plasticfreewaters.entity;

import com.google.gson.Gson;

import org.rotary.iyfr.plasticfreewaters.service.ApplicationMonitor;

public class Device {

    public String _OSVERSION = System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")";
    public String _RELEASE = android.os.Build.VERSION.RELEASE;
    public String _DEVICE = android.os.Build.DEVICE;
    public String _MODEL = android.os.Build.MODEL;
    public String _PRODUCT = android.os.Build.PRODUCT;
    public String _BRAND = android.os.Build.BRAND;
    public String _DISPLAY = android.os.Build.DISPLAY;
    public String _CPU_ABI = android.os.Build.CPU_ABI;
    public String _CPU_ABI2 = android.os.Build.CPU_ABI2;
    public String _UNKNOWN = android.os.Build.UNKNOWN;
    public String _HARDWARE = android.os.Build.HARDWARE;
    public String _ID = android.os.Build.ID;
    public String _MANUFACTURER = android.os.Build.MANUFACTURER;
    public String _SERIAL = android.os.Build.SERIAL;
    public String _USER = android.os.Build.USER;
    public String _HOST = android.os.Build.HOST;

    public static String getDeviceSuperInfo() {
        String deviceSuperInfo = "";
        try {
            Device device = new Device();
            Gson gson = new Gson();
            deviceSuperInfo = gson.toJson(device, Device.class);
            return deviceSuperInfo;
        } catch (Exception e) {
            e.printStackTrace();
            ApplicationMonitor.logFirebaseCrashlyticsEvent(e);
        } finally {
            return deviceSuperInfo;
        }
    }
}