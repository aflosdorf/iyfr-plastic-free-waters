package org.rotary.iyfr.plasticfreewaters.ui;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.service.AlertBuilder;
import org.rotary.iyfr.plasticfreewaters.service.SharedPreferencesEditor;
import org.rotary.iyfr.plasticfreewaters.service.ConfigurationProxy;

public class OnboardingActivity extends AppCompatActivity {

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConfigurationProxy.REQUESTCODE_CHOOSE_ACCOUNT) {
            if (resultCode == Activity.RESULT_OK) {
                /* Find out and save the email address or identity */
                String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                SharedPreferencesEditor.saveUserIdentity(accountName);
                /* Get and save the edited name of the user */
                TextInputEditText textFieldName = findViewById(R.id.textFieldName);
                SharedPreferencesEditor.saveUserName(textFieldName.getText().toString().trim());

                // String accountType = data.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);
                // Account[] accounts = AccountManager.get(this).getAccountsByType(accountType);

                /* Start the application */
                startActivity(new Intent(getApplicationContext(), GoogleMapsActivity.class));
                finish();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        TextInputEditText textFieldName = findViewById(R.id.textFieldName);
        textFieldName.requestFocus();
        textFieldName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35), ConfigurationProxy.symbolFilter});
        textFieldName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                TextInputLayout textInputLayoutName = findViewById(R.id.textInputLayoutName);
                Button buttonLinkAccount = findViewById(R.id.buttonLinkAccount);
                if (s.toString().trim().isEmpty()) {
                    textInputLayoutName.setErrorEnabled(true);
                    textInputLayoutName.setError(getString(R.string.onboarding_enter_name_helper_text));
                    buttonLinkAccount.setEnabled(false);
                } else {
                    textInputLayoutName.setErrorEnabled(false);
                    textInputLayoutName.setError(null);
                    buttonLinkAccount.setEnabled(true);
                }
            }
        });
    }

    public void onClickFabSelectAccount(View view) {
        Intent intent = AccountManager.newChooseAccountIntent(null, null, new String[]{"com.google", "com.google.android.legacyimap"}, null, null, null, null);
        startActivityForResult(intent, ConfigurationProxy.REQUESTCODE_CHOOSE_ACCOUNT);
    }

    public void onClickWhyToLinkAccount(View view) {
        AlertBuilder.showReasonWhyAccountIsNeeded(this);
    }
}