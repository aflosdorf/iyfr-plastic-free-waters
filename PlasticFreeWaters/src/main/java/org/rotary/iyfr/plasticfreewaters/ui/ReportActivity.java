package org.rotary.iyfr.plasticfreewaters.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListView;
import com.google.android.material.textfield.TextInputEditText;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.entity.Gpx;
import org.rotary.iyfr.plasticfreewaters.entity.Report;
import org.rotary.iyfr.plasticfreewaters.entity.Waypoint;
import org.rotary.iyfr.plasticfreewaters.service.AlertBuilder;
import org.rotary.iyfr.plasticfreewaters.service.Alerting;
import org.rotary.iyfr.plasticfreewaters.service.App;
import org.rotary.iyfr.plasticfreewaters.service.ApplicationMonitor;
import org.rotary.iyfr.plasticfreewaters.service.ConfigurationProxy;
import org.rotary.iyfr.plasticfreewaters.service.LocalFileStorageService;
import org.rotary.iyfr.plasticfreewaters.service.RemoteFileStorageService;
import org.rotary.iyfr.plasticfreewaters.service.SharedPreferencesEditor;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class ReportActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static String INTENT_EXTRA_LOCATION = "intent-extra-location";
    public static String INTENT_EXTRA_REPORT_ID = "intent-extra-report-id";

    Report report = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        getSupportActionBar().setTitle(getString(R.string.repot_title));
        App.setReportActivity(this);

        /* Read Intent Extras and get the right report object */
        Location location = getIntent().getParcelableExtra(INTENT_EXTRA_LOCATION);
        long reportId = getIntent().getLongExtra(INTENT_EXTRA_REPORT_ID, 0);
        boolean isNewReport = reportId == 0 ? true : false;
        report = isNewReport ? new Report(new Waypoint(location)) : LocalFileStorageService.readReport(String.valueOf(reportId));
        report.toCache();

        /* UI - location, date and time */
        TextInputEditText textFieldReportLocation = findViewById(R.id.textFieldReportLocation);
        textFieldReportLocation.setText(report.getNameForUi(" | ") + "\n" + report.getLocalDateAndTime());
        textFieldReportLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });

        /* UI - category */
        ArrayAdapter<CharSequence> adapterCategory = new ArrayAdapter<>(this, R.layout.cat_exposed_dropdown_popup_item, // Hint: This is a custom copy of 'cat_exposed_dropdown_popup_item'
                getResources().getStringArray(R.array.report_category_list));
        AutoCompleteTextView editTextFilledExposedDropdownCategory = findViewById(R.id.outlined_exposed_dropdown_category);
        editTextFilledExposedDropdownCategory.setAdapter(adapterCategory);
        editTextFilledExposedDropdownCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {/* */}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {/* */ }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                report.setCategory(s.toString().trim());
                report.toCache();
                report.setSaved(false);
            }
        });

        /* UI - dimension */
        ArrayAdapter<CharSequence> adapterDimension = new ArrayAdapter<>(this, R.layout.cat_exposed_dropdown_popup_item, // Hint: This is a custom copy of 'cat_exposed_dropdown_popup_item'
                getResources().getStringArray(R.array.report_dimension_list));
        AutoCompleteTextView editTextFilledExposedDropdownDimension = findViewById(R.id.outlined_exposed_dropdown_dimension);
        editTextFilledExposedDropdownDimension.setAdapter(adapterDimension);
        editTextFilledExposedDropdownDimension.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {/* */}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {/* */ }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                report.setDimension(s.toString().trim());
                report.toCache();
                report.setSaved(false);
            }
        });

        /* UI - depth */
        ArrayAdapter<CharSequence> adapterDepth = new ArrayAdapter<>(this, R.layout.cat_exposed_dropdown_popup_item, // Hint: This is a custom copy of 'cat_exposed_dropdown_popup_item'
                getResources().getStringArray(R.array.report_depth_list));
        AutoCompleteTextView editTextFilledExposedDropdownDepth = findViewById(R.id.outlined_exposed_dropdown_depth);
        editTextFilledExposedDropdownDepth.setAdapter(adapterDepth);
        editTextFilledExposedDropdownDepth.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {/* */ }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {/* */ }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                report.setDepth(s.toString().trim());
                report.toCache();
                report.setSaved(false);
            }
        });

        /* UI - description */
        TextInputEditText textFieldReportDescription = findViewById(R.id.textFieldReportDescription);
        /* Der ConfigurationProxy.symbolFilter hat ungewollte Eingabeartefakte auf einem italienischen Gerät verursacht. */
        textFieldReportDescription.setFilters(new InputFilter[]{new InputFilter.LengthFilter(500)});
        textFieldReportDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });
        textFieldReportDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {/* */}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {/* */}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                report.setDescription(s.toString());
                report.toCache();
                report.setSaved(false);
            }
        });

        /* UI - pictures */
        ReportActivity activity = this;
        MaterialListView listViewReportPictures = findViewById(R.id.listViewReportPictures);
        listViewReportPictures.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull Card card, int position) {
                /* Intent to view picture outside this app */
                String pictureFilePath = (String) card.getTag();
                Uri uri = FileProvider.getUriForFile(App.context(), getApplicationContext().getPackageName() + ".fileprovider", new File(pictureFilePath));
                Intent viewPictureIntent = new Intent(Intent.ACTION_VIEW);
                viewPictureIntent.setData(uri);
                viewPictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                activity.startActivity(viewPictureIntent);
            }

            @Override
            public void onItemLongClick(@NonNull Card card, int position) {
                /* Delete picture from this report */
                Alerting.playVibrator();
                String pictureFilePath = (String) card.getTag();
                AlertBuilder.showDeletePicture(card, pictureFilePath, activity);
            }
        });

        /* UI - set specific field values */
        if (isNewReport) {
            /* Initialise the UI. */
            editTextFilledExposedDropdownCategory.requestFocus();
            App.getGoogleMapsActivity().takeScreenshot(report.getId());
        } else {
            /* Initialise the UI with data from existing report. */
            editTextFilledExposedDropdownCategory.setText(report.getCategory(), false);
            editTextFilledExposedDropdownDimension.setText(report.getDimension(), false);
            editTextFilledExposedDropdownDepth.setText(report.getDepth(), false);
            textFieldReportDescription.setText(report.getDescription());
            updatePictureScollView();
        }

        /* Register this 'ReportActivity' as SharedPreferencesListener */
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onBackPressed() {
        if (report.isSaved()) {
            Report.reset();
            finish();
        } else
            AlertBuilder.showDiscardChanges(this);
    }

    public void onClickFabAddPicture(View view) {
        /* Cache report to be able to access it in the CameraActivity */
        report.toCache();
        Intent intentCameraActivity = new Intent(this, CameraActivity.class);
        startActivity(intentCameraActivity);
    }

    public void onClickFabShare(View view) {
        /* Input validation - We want the user to give an aproximation about category,dimension an ddepth. */
        AutoCompleteTextView editTextFilledExposedDropdownCategory = findViewById(R.id.outlined_exposed_dropdown_category);
        String category = editTextFilledExposedDropdownCategory.getText().toString().trim();
        AutoCompleteTextView editTextFilledExposedDropdownDimension = findViewById(R.id.outlined_exposed_dropdown_dimension);
        String dimension = editTextFilledExposedDropdownDimension.getText().toString().trim();
        AutoCompleteTextView editTextFilledExposedDropdownDepth = findViewById(R.id.outlined_exposed_dropdown_depth);
        String depth = editTextFilledExposedDropdownDepth.getText().toString().trim();
        String report_category_list_firstEntry = Arrays.stream(getResources().getStringArray(R.array.report_category_list)).iterator().next();

        if (category.equals("")) {
            AlertBuilder.showInputValidation(getString(R.string.report_category_hint));
            editTextFilledExposedDropdownCategory.requestFocus();
            return;
        } else if (dimension.equals("")) {
            AlertBuilder.showInputValidation(getString(R.string.report_dimension_hint));
            editTextFilledExposedDropdownDimension.requestFocus();
            return;
        } else if (depth.equals("") && !category.equals(report_category_list_firstEntry)) {
            AlertBuilder.showInputValidation(getString(R.string.report_depth_hint));
            editTextFilledExposedDropdownDepth.requestFocus();
            return;
        }
        /* If everything is fine, we go on and show some hints before we share the report. */
        SharedPreferencesEditor sharedPreferencesEditor = new SharedPreferencesEditor();
        if (sharedPreferencesEditor.isDoShareHint()) {
            AlertBuilder.showShareHint(this);
        } else {
            saveAndShareReport();
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        /*
         * This event is fired in {@link CameraActivity}, when a new picture is taken and saved.
         */
        if (key.equals(App.context().getString(R.string.triggerUpdatePictureList))) {
            updatePictureScollView();
        }
    }

    public void saveAndShareReport() {
        /* To ensure consistent data inside the app, we save the report first. */
        saveReport();
        /* Save report to central remote storage (firebase storage of project 'Plastic free waters App' ) */
        RemoteFileStorageService.saveReportToRemoteStorageAsync(this, report.getId());
        /* Share report */
        shareReport(report);
        report.setSend(true);
        Report.reset();
        finish();
    }

    public void saveReport() {
        String author = SharedPreferencesEditor.getUserName() + " (" + SharedPreferencesEditor.getUserIdentity() + ")";
        /* Save the collected Data to gpx file */
        Gpx gpx = new Gpx(report.getNameForGpx(), report.getCategory(), report.getDescription(), author, report.getWaypoint());
        String gpxContent = gpx.createGpxContent();
        LocalFileStorageService.saveToReportFolder(String.valueOf(report.getId()), gpxContent, LocalFileStorageService.FILE_EXTENSION_GPX);
        /* Save the collected Data to json file */
        String jsonContent = report.toJson();
        LocalFileStorageService.saveToReportFolder(String.valueOf(report.getId()), jsonContent, LocalFileStorageService.FILE_EXTENSION_JSON);
        report.setSaved(true);
        /* User information */
        Toast.makeText(getApplicationContext(), getString(R.string.report_saved), Toast.LENGTH_SHORT).show();
    }

    public void shareReport(Report report) {
        /* If there are multiple files in the report (gpx file and picture(s)), the options
         * in the share intent differ in order to the szenario (one attachment or multiple). */
        Intent intent = null;
        if (report.hasPictures())
            intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        else
            intent = new Intent(Intent.ACTION_SEND);
        intent.setType("vnd.android.cursor.dir/email");

        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{ConfigurationProxy.getEMailAdressOfAuthority()});
        String userIdentityEmail = SharedPreferencesEditor.getUserIdentity();
        if (ConfigurationProxy.isValidEmail(userIdentityEmail))
            intent.putExtra(Intent.EXTRA_CC, new String[]{userIdentityEmail});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.repot_title));
        /**
         * Dear Sir or Madam,
         * I am sending you a new "floating object report" with the following details:
         *
         * Location: N 50°42'10,10196"|E 6°48'56,17116" (50.7028061, 6.8156031)
         * Date/Time:
         * Category: Drifting net
         * Description:
         * Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist ...
         *
         * Please have a look at the attachments to this mail, there you will find a GPX file with all the information and possibly visual material.
         *
         * Best regards,
         * Andreas (andreas.flosdorf@gmail.com)
         *
         */
        String emailText = getString(R.string.repot_email_text,
                report.getNameForUi("|") + " (" + report.getWaypoint().getLatLngFormatted() + ")",
                report.getLocalDateAndTime(),
                report.getCategory(),
                report.getDimension(),
                report.getDepth(),
                report.getDescription(),
                report.getNotifier());
        intent.putExtra(Intent.EXTRA_TEXT, emailText);
        /* ATTACHMENTS */
        ArrayList<Uri> uris = new ArrayList<Uri>();
        /* Get Uri for the saved gpx file --> INFO: Wird nicht mit der EMail versendet. */
        // String gpxFilePath = LocalFileStorageService.getReportFolderPath(report.getId()) + File.separator + report.getId() + LocalFileStorageService.FILE_EXTENSION_GPX;
        // Uri uri = FileProvider.getUriForFile(App.context(), getApplicationContext().getPackageName() + ".fileprovider", new File(gpxFilePath));
        /* Get Uri(s) for the saved pictures, if exists */
        if (report.hasPictures()) {
            ArrayList<String> pictures = report.getPictures();
            for (String picture : pictures) {
                Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".fileprovider", new File(picture));
                uris.add(uri);
            }
        }
        /* Uri's als Stream an die Mail anhängen */
        if (uris.size() > 1)
            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        else if (uris.size() == 1)
            intent.putExtra(Intent.EXTRA_STREAM, uris.get(0));
        /* Uri Permissions mitgeben */
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        /* Chooser starten, der EMail App anbietet */
        startActivity(Intent.createChooser(intent, getString(R.string.repot_title)));
    }

    public void updatePictureScollView() {
        report.setSaved(false);
        File path = new File(getApplicationContext().getExternalFilesDir(null), "reports/" + report.getId() + "/");
        File[] files = path.listFiles();
        ArrayList<String> reportPictures = new ArrayList<String>();

        if (files != null && files.length > 0) {
            MaterialListView listViewReportPictures = findViewById(R.id.listViewReportPictures);
            listViewReportPictures.getAdapter().clearAll();
            listViewReportPictures.setVisibility(View.VISIBLE);

            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                /* In the same folder may exist a *.gpx file. */
                if (!file.getName().endsWith(LocalFileStorageService.FILE_EXTENSION_JPG)) continue;
                reportPictures.add(file.getPath());
                Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
                Drawable drawable = new BitmapDrawable(bitmap);
                try {
                    Card card = new Card.Builder(this)
                            .setTag(file.getPath())
                            .withProvider(new CardProvider())
                            .setLayout(R.layout.material_big_image_card_layout_custom) // Hint: This is a custom copy of 'material_big_image_card_layout'
                            .setDrawable(drawable)
                            .endConfig()
                            .build();
                    listViewReportPictures.getAdapter().add(card);

                } catch (Exception e) {
                    e.printStackTrace();
                    ApplicationMonitor.logFirebaseCrashlyticsEvent(e);
                }
            }
            report.setPictures(reportPictures);
            report.toCache();
        }
    }
}