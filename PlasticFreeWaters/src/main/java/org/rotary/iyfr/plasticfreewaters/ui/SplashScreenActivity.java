package org.rotary.iyfr.plasticfreewaters.ui;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.service.SharedPreferencesEditor;
import org.rotary.iyfr.plasticfreewaters.entity.Report;
import org.rotary.iyfr.plasticfreewaters.entity.States;
import org.rotary.iyfr.plasticfreewaters.service.App;
import org.rotary.iyfr.plasticfreewaters.service.ApplicationMonitor;
import org.rotary.iyfr.plasticfreewaters.service.LocalFileStorageService;

public class SplashScreenActivity extends AppCompatActivity {

    private static final int ANIMATION_ROTATION = 360;
    private static final int ANIMATION_DELAY = 1500;
    private ApplicationMonitor applicationMonitor = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        /* Set the version information properly */
        TextView textViewAppVersion = findViewById(R.id.textViewAppVersion);
        textViewAppVersion.setText(App.getAppVersionWithVersionCode());
        /* Abrufen, ob Test-Modus aktiviert ist. */
        States.setTest(ApplicationMonitor.isTest());
        /* Reset some states at startup. */
        Report.reset();
        /* Initialisiert FirebaseAnalytics und FirebaseRemoteConfig */
        applicationMonitor = new ApplicationMonitor();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*-  Kleine Animation :)  -*/
        final ImageView imageViewIcon = findViewById(R.id.imageViewRotaryIcon);
        imageViewIcon.animate().rotation(ANIMATION_ROTATION).setDuration(ANIMATION_DELAY).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationCancel(Animator animation) {/* NIX */ }

            @Override
            public void onAnimationEnd(Animator animation) {
                /* Check if a user account is already known */
                boolean hasKnownUser = SharedPreferencesEditor.getUserIdentity() != null;
                /* If no user account is already known, we ask the user to choose or create a valid google account.
                 * Otherwise we start the app like usual. */
                if (hasKnownUser) {
                    startActivity(new Intent(getApplicationContext(), GoogleMapsActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), OnboardingActivity.class));
                    finish();
                }
                /* Sorge dafür, dass das Dateisystem alle Voraussetzungen erfüllt. */
                LocalFileStorageService.organizeDataFilesDirectory();
            }

            @Override
            public void onAnimationRepeat(Animator animation) {/* NIX */ }

            @Override
            public void onAnimationStart(Animator animation) {/* NIX */ }
        }).start();
    }
}