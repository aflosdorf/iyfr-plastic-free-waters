package org.rotary.iyfr.plasticfreewaters.service;

import android.app.IntentService;
import android.content.Intent;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.rotary.iyfr.plasticfreewaters.entity.States;
import org.rotary.iyfr.plasticfreewaters.ui.ReportActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Locale;

/**
 * Speichert (wenn konfiguriert) einen Report (GPX & JPGs) in Firebase Storage.
 */
public class RemoteFileStorageService extends IntentService {

    private static final String INTENT_EXTRA_REPORT_ID = "reportId";
    private static final String REMOTE_DIRECTORY_REPORTS = "reports";

    /**
     * Default Konstruktor wird an einem Service erwartet.
     * Der konstante String wird lediglich zu Debug-Zwecken benötigt.
     */
    public RemoteFileStorageService() {
        super("RemoteFileStorageService");
    }

    /**
     * Speichert einen Report asynchron im Remote Storage ab. Dazu gehört folgende Struktur und Inhalte:
     * - Speicherpfad: /reports/languageCode/reportId/files
     * - Dateien sind: 1 x gpx, n x jpg Dateien
     */
    public static void saveReportToRemoteStorageAsync(ReportActivity context, long reportId) {
        Intent remoteStorageService = new Intent(context, RemoteFileStorageService.class);
        remoteStorageService.putExtra(RemoteFileStorageService.INTENT_EXTRA_REPORT_ID, reportId);
        context.startService(remoteStorageService);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (ConfigurationProxy.isSaveReportToStorage()) {
            /* Remote File Path definieren */
            String directory = REMOTE_DIRECTORY_REPORTS;
            if (States.isTest())
                directory = "test-" + directory;

            /* Report Path & Inhalte sammeln */
            String languageCode = Locale.getDefault().getLanguage();
            long reportId = intent.getLongExtra(INTENT_EXTRA_REPORT_ID, System.currentTimeMillis());
            final String reportPath = directory + "/" + languageCode + "/" + reportId + "/";

            /* Dateiübertragung nach Firebase Storage */
            StorageReference storageReference = FirebaseStorage.getInstance().getReference();
            /* Alle Dateien eines Reports finden und in den Firebase Storage Stream legen. */
            String reportFolderPath = LocalFileStorageService.getReportFolderPath(reportId);
            File reportDirectory = new File(reportFolderPath);
            String[] reportFilesList = reportDirectory.list();
            for (int i = 0; i < reportFilesList.length; i++) {
                try {
                    /* Lokale Datei */
                    String filePath = reportFilesList[i];
                    File file = new File(reportDirectory + File.separator + filePath);
                    /* Remote Datei */
                    StorageReference childReferenceHistory = storageReference.child(reportPath + file.getName());
                    FileInputStream fileInputStream = null;
                    fileInputStream = new FileInputStream(file);
                    childReferenceHistory.putStream(fileInputStream);
                } catch (FileNotFoundException e) {
                    ApplicationMonitor.logFirebaseCrashlyticsEvent(e);
                }
            }
        }
    }
}
