package org.rotary.iyfr.plasticfreewaters.service;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.entity.Report;
import org.rotary.iyfr.plasticfreewaters.service.App;

public class SharedPreferencesEditor {

    public static String getNotifier() {
        String notifier = getUserName() + " (" + getUserIdentity() + ")";
        return notifier;
    }

    public static Report getReport() {
        String key = App.context().getString(R.string.saveReport);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        String json = sharedPreferences.getString(key, "null");
        if (json.contains("null")) return null;
        else {
            Gson gson = new Gson();
            Report report = gson.fromJson(json, Report.class);
            return report;
        }
    }

    public static String getUserIdentity() {
        String key = App.context().getString(R.string.saveUserIdentity);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        String userIdentity = sharedPreferences.getString(key, "null");
        if (userIdentity.contains("null")) return null;
        else return userIdentity;
    }

    public static String getUserName() {
        String key = App.context().getString(R.string.saveUserName);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        String userName = sharedPreferences.getString(key, "null");
        if (userName.contains("null")) return null;
        else return userName;
    }

    public static void saveReport(Report report) {
        Gson gson = new Gson();
        String json = "null";
        if (report != null) json = gson.toJson(report, Report.class);
        String key = App.context().getString(R.string.saveReport);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        sharedPreferences
                .edit()
                .putString(key, json)
                .commit();
    }

    public static void saveUserIdentity(String userIdentity) {
        String key = App.context().getString(R.string.saveUserIdentity);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        sharedPreferences
                .edit()
                .putString(key, userIdentity)
                .apply();
    }

    public static void saveUserName(String userName) {
        String key = App.context().getString(R.string.saveUserName);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        sharedPreferences
                .edit()
                .putString(key, userName)
                .apply();
    }

    public static void triggerUpdateReportPictureGalery() {
        String key = App.context().getString(R.string.triggerUpdatePictureList);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        sharedPreferences
                .edit()
                .putString(key, String.format("%d", System.currentTimeMillis()))
                .apply();
    }

    public boolean isAnalyticsCollectionEnabled() {
        String key = App.context().getString(R.string.settings_usability_improvement_program_key);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        return sharedPreferences.getBoolean(key, true);
    }

    public boolean isDoShareHint() {
        String key = App.context().getString(R.string.do_alert_share_hint_email);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        return sharedPreferences.getBoolean(key, true);
    }

    public void setDoShareHint(boolean trueFalse) {
        String key = App.context().getString(R.string.do_alert_share_hint_email);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.context());
        sharedPreferences
                .edit()
                .putBoolean(key, trueFalse)
                .apply();
    }
}
