package org.rotary.iyfr.plasticfreewaters.service;

import android.graphics.Bitmap;
import android.net.Uri;

import com.google.gson.Gson;

import org.rotary.iyfr.plasticfreewaters.entity.Report;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class LocalFileStorageService {

    public static final String FOLDER_REPORTS = "reports/";
    public static final String FILE_EXTENSION_JPG = ".jpg";
    public static final String FILE_EXTENSION_GPX = ".gpx";
    public static final String FILE_EXTENSION_JSON = ".json";

    public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        fileOrDirectory.delete();
    }

    public static void deleteReport(Report report) {
        try {
            File directory = getBasePathFor(FOLDER_REPORTS + report.getId(), null);
            deleteRecursive(directory);
        } catch (Exception e) {
            e.printStackTrace();
            ApplicationMonitor.logFirebaseCrashlyticsEvent(e);
        }
    }

    public static boolean doesFileExistInFolder(String directory, String fileName) {
        File folder = getBasePathFor(directory, null);
        boolean exists = false;
        try {
            if (folder.isDirectory()) {
                String[] fileList = folder.list();
                for (String s : fileList)
                    if (s.equals(fileName)) exists = Boolean.TRUE;
            }
        } catch (Exception e) {
            ApplicationMonitor.logFirebaseCrashlyticsEvent(e);
        } finally {
            return exists;
        }
    }

    private static String extractStringFromUri(Uri uri) throws OutOfMemoryError {
        String fileAsString = "null";
        InputStream inputStream = null;
        try {
            /* Siehe: https://stackoverflow.com/questions/309424/how-do-i-read-convert-an-inputstream-into-a-string-in-java
             * Hier werden die verschiedenen Techniken aufgezählt, mit denen das Einlesen von Strings möglich ist.
             * Im Folgenden wird die Variante 8 ('Using ByteArrayOutputStream and inputStream.read (JDK)') verwendet,
             * Vorgänger war mittels StringBuilder, der schon einmal in OOM gelaufen ist, da er die dynamische
             * Speichererweiterung mit (capacity *2)+2 steuert.
             * Dies ist mit großen Strings ein Problem, da große capacitys dadurch pauschal doppelt so groß werden. */
            inputStream = App.context().getContentResolver().openInputStream(uri);
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while (inputStream != null && (length = inputStream.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            fileAsString = result.toString("UTF-8");
        } catch (IOException e) {
            /* Bereits in Crashlytics bewertet und zunächst ausgeblendet.
               --> android intent-filter open failed: EACCES (Permission denied)
               --> Siehe auch: https://medium.com/@sriramaripirala/android-10-open-failed-eacces-permission-denied-da8b630a89df */
            String cause = "";
            try { // Das hier gab in Ausnahmefällen eine neue Exception (Crashlytics), daher fangen.
                cause = e.getCause().toString();
            } catch (Exception nix) {/* NIX */}
            if (cause.toLowerCase().contains("eacces") ||
                    cause.toLowerCase().contains("permission") ||
                    cause.toLowerCase().contains("denied"))
                throw new SecurityException("URI: " + uri.toString(), e);
            else
                ApplicationMonitor.logFirebaseCrashlyticsEvent(new IOException("URI: " + uri.toString(), e));
        }
        return fileAsString;
    }

    /**
     * Determines the file representation of a path within the app's data directory.
     * Example: /Android/data/package/directory/file
     *
     * @return File represents the specified path to a folder or file
     */
    public static File getBasePathFor(String directory, String file) {
        if (directory == null && file == null) return App.context().getExternalFilesDir(null);
        else if (directory != null && file == null)
            return new File(App.context().getExternalFilesDir(null), directory);
        else if (directory == null && file != null)
            return new File(App.context().getExternalFilesDir(null), file);
        else {
            File path = getBasePathFor(directory, null);
            return new File(path, file);
        }
    }

    public static String getReportFolderPath(long reportId) {
        File reportDirectory = getBasePathFor(FOLDER_REPORTS + reportId, null);
        return reportDirectory.getPath();
    }

    public static ArrayList<String> listReportFolder() {
        File directory = getBasePathFor(FOLDER_REPORTS, null);
        FilenameFilter filenameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if (dir.isDirectory()) return true;
                else return false;
            }
        };
        String[] listReportsFolder = directory.list(filenameFilter);
        ArrayList<String> directories = new ArrayList<String>();
        /* Suchen nach Reports-Ordnern und schauen, ob sie tatsächlich Reports enthalten.
         * Enthalten sie keine gpx Dateien, werden die Ordner gelöscht, damit Karteileichen später
         * nicht zu ungewollten Nebeneffekten führen. */
        for (int i = 0; i < listReportsFolder.length; i++) {
            File entry = new File(directory + File.separator + listReportsFolder[i]);
            String[] entryListing = entry.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(LocalFileStorageService.FILE_EXTENSION_GPX);
                }
            });
            if (entryListing == null || entryListing.length == 0)
                deleteRecursive(entry);
            else directories.add(listReportsFolder[i]);
        }
        return directories;
    }

    public static void organizeDataFilesDirectory() {
        /* Verzeichnisse prüfen und ggf. anlegen. */
        File directoryRecordings = getBasePathFor(FOLDER_REPORTS, null);
        if (!directoryRecordings.exists())
            directoryRecordings.mkdir();
    }

    private static String readContentAsStringFromUri(Uri uri) {
        String fileAsString;
        try {
            fileAsString = extractStringFromUri(uri);
        } catch (OutOfMemoryError e1) {
            /* Wir fangen den OOM und versuchen den Garbage Collector aufräumen zu lassen.
             * Im Anschluss versuchen wir es noch einmal. */
            System.gc();
            try {
                fileAsString = extractStringFromUri(uri);
            } catch (OutOfMemoryError e2) {
                ApplicationMonitor.logFirebaseCrashlyticsEvent(new Exception(e2));
                return "outofmemory";
            } catch (SecurityException s2) {
                ApplicationMonitor.logFirebaseCrashlyticsEvent(s2);
                return "permissiondenial";
            }
        } catch (SecurityException s1) {
            ApplicationMonitor.logFirebaseCrashlyticsEvent(s1);
            return "permissiondenial";
        }
        return fileAsString;
    }

    public static Report readReport(String reportFolder) {
        /* We get basic information for our report from the gpx file... */
        File directory = getBasePathFor(FOLDER_REPORTS + reportFolder, null);
        File jsonFile = new File(directory, reportFolder + FILE_EXTENSION_JSON);
        String jsonContent = LocalFileStorageService.readContentAsStringFromUri(Uri.fromFile(jsonFile));
        Gson gson = new Gson();
        Report report = gson.fromJson(jsonContent, Report.class);

        /* ... and we add information about the pictures. */
        String[] pictureList = directory.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if (name.endsWith(FILE_EXTENSION_JPG)) return true;
                else return false;
            }
        });
        if (pictureList != null && pictureList.length > 0) {
            ArrayList<String> pictures = new ArrayList<String>();
            for (int i = 0; i < pictureList.length; i++)
                pictures.add(pictureList[i]);
            report.setPictures(pictures);
        }
        report.setSaved(true);
        return report;
    }

    public static boolean saveScreenshot(long recordingsFolder, Bitmap bitmap) {
        boolean success = false;
        try {
            File path = LocalFileStorageService.getBasePathFor(LocalFileStorageService.FOLDER_REPORTS, String.valueOf(recordingsFolder));
            if (!path.exists()) path.mkdirs();
            File picture = new File(path, "screenshot-" + recordingsFolder + ".jpg");
            BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(picture));
            success = bitmap.compress(Bitmap.CompressFormat.JPEG, 15, outStream);
            outStream.flush();
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return success;
        }
    }

    public static String saveToReportFolder(String reportFolder, String content, String fileExtension) {
        try {
            File directory = getBasePathFor(FOLDER_REPORTS + reportFolder, null);
            if (!directory.exists()) directory.mkdirs();
            File file = new File(directory, reportFolder + fileExtension);
            if (file.exists()) file.delete();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(content.getBytes());
            fileOutputStream.close();
            return file.getPath();
        } catch (Exception e) {
            e.printStackTrace();
            ApplicationMonitor.logFirebaseCrashlyticsEvent(e);
        }
        return reportFolder;
    }
}