package org.rotary.iyfr.plasticfreewaters.service;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import org.rotary.iyfr.plasticfreewaters.ui.GoogleMapsActivity;
import org.rotary.iyfr.plasticfreewaters.ui.ReportActivity;

/**
 * This app is build for the operation 'PLASTIC FREE WATERS' that ist founded by members of the International Yachting Fellowship of Rotarians
 * and by thousands of Rotarian Mariners from around the world.
 * The basic idea of this app is to document pollution in the sea and report it to official authorities in order to clean it up.
 * <p>
 * The features of this app are:
 * - one's own location or a location marked on the Google map is set as the position (latitude/longitude) of a pollution.
 * - A report can be generated that can have a name, description, images, author and timestamp associated with the specified location.
 * - The report can be saved, edited later and shared.
 * - Sharing the report can be done in different ways. For example, sending it by email to an official authority. The report can also be stored
 * as files (gpx and jpg) in the file system or in a cloud and sent from there.
 * - Images are geotagged (EXIF)
 * <p>
 * The technologies used are as follows:
 * - Native App Development - Android versions 8.0 (API 26) - 11.0 (API 30)
 * - Google Play Store
 * - Firebase (Authentication, Crashlytics, Performance, Storage, Analytics)
 * - Google Cloud
 */
public class App extends Application {

    private static Context context;
    private static GoogleMapsActivity googleMapsActivity;
    private static ReportActivity reportActivity;

    public static Context context() {
        return App.context;
    }

    public static String getAppVersionWithVersionCode() {
        return getVersion() + " (" + getVersionCode() + ")";
    }

    public static GoogleMapsActivity getGoogleMapsActivity() {
        return App.googleMapsActivity;
    }

    public static ReportActivity getReportActivity() {
        return App.reportActivity;
    }

    public static String getVersion() {
        String version = "";
        try {
            PackageInfo packageInfo = context().getPackageManager().getPackageInfo(context().getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public static int getVersionCode() {
        try {
            PackageInfo packageInfo = context().getPackageManager().getPackageInfo(context().getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return Integer.MAX_VALUE;
    }

    public static void setGoogleMapsActivity(GoogleMapsActivity googleMapsActivity) {
        App.googleMapsActivity = googleMapsActivity;
    }

    public static void setReportActivity(ReportActivity reportActivity) {
        App.reportActivity = reportActivity;
    }

    public void onCreate() {
        super.onCreate();
        App.context = getApplicationContext();
    }
}
