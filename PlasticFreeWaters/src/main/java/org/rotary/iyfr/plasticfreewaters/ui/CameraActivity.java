package org.rotary.iyfr.plasticfreewaters.ui;

import android.media.ExifInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.FileCallback;
import com.otaliastudios.cameraview.PictureResult;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.service.SharedPreferencesEditor;
import org.rotary.iyfr.plasticfreewaters.entity.Waypoint;
import org.rotary.iyfr.plasticfreewaters.service.ApplicationMonitor;
import org.rotary.iyfr.plasticfreewaters.service.LocalFileStorageService;

import java.io.File;
import java.io.IOException;

public class CameraActivity extends AppCompatActivity {

    private CameraView camera;

    static public void setGeoTag(String picture, double latitude, double longitude) {
        /* These information can be extracted for example by https://geosetter.de/ in batch mode to visualise in google maps. */
        try {
            ExifInterface exif = new ExifInterface(picture);

            int num1Lat = (int) Math.floor(latitude);
            int num2Lat = (int) Math.floor((latitude - num1Lat) * 60);
            double num3Lat = (latitude - ((double) num1Lat + ((double) num2Lat / 60))) * 3600000;

            int num1Lon = (int) Math.floor(longitude);
            int num2Lon = (int) Math.floor((longitude - num1Lon) * 60);
            double num3Lon = (longitude - ((double) num1Lon + ((double) num2Lon / 60))) * 3600000;

            String lat = num1Lat + "/1," + num2Lat + "/1," + num3Lat + "/1000";
            String lon = num1Lon + "/1," + num2Lon + "/1," + num3Lon + "/1000";

            if (latitude > 0) {
                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N");
            } else {
                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
            }
            if (longitude > 0) {
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");
            } else {
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
            }

            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, lat);
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, lon);
            exif.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
            ApplicationMonitor.logFirebaseCrashlyticsEvent(e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        camera = findViewById(R.id.camera);
        camera.setLifecycleOwner(this);
        camera.setSnapshotMaxWidth(1500);
        camera.setSnapshotMaxHeight(1500);
        camera.addCameraListener(new CameraListener() {
            @Override
            public void onCameraError(@NonNull CameraException e) {
                super.onCameraError(e);
                ApplicationMonitor.logFirebaseCrashlyticsEvent(e);
            }

            @Override
            public void onPictureTaken(PictureResult result) {
                /* If planning to save a file on a background thread, just use toFile. Ensure you have permissions. */
                File path = new File(getApplicationContext().getExternalFilesDir(null), "reports/" + SharedPreferencesEditor.getReport().getId());
                if (!path.exists())
                    path.mkdirs();
                File savedPicture = new File(path, "picture" + "-" + System.currentTimeMillis() + LocalFileStorageService.FILE_EXTENSION_JPG);
                result.toFile(savedPicture, new FileCallback() {
                    @Override
                    public void onFileReady(@Nullable File file) {
                        Waypoint waypoint = SharedPreferencesEditor.getReport().getWaypoint();
                        setGeoTag(file.getPath(), waypoint.getLatitude(), waypoint.getLongitude());
                        SharedPreferencesEditor.triggerUpdateReportPictureGalery();
                        finish();
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        camera.destroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        camera.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera.open();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onClickFabCapture(View view) {
        camera.takePictureSnapshot();
    }
}