package org.rotary.iyfr.plasticfreewaters.service;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.CheckBox;

import com.dexafree.materialList.card.Card;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.entity.Report;
import org.rotary.iyfr.plasticfreewaters.service.App;
import org.rotary.iyfr.plasticfreewaters.service.ConfigurationProxy;
import org.rotary.iyfr.plasticfreewaters.service.LocalFileStorageService;
import org.rotary.iyfr.plasticfreewaters.service.SharedPreferencesEditor;
import org.rotary.iyfr.plasticfreewaters.ui.GoogleMapsActivity;
import org.rotary.iyfr.plasticfreewaters.ui.OnboardingActivity;
import org.rotary.iyfr.plasticfreewaters.ui.ReportActivity;
import org.rotary.iyfr.plasticfreewaters.ui.ReportOverviewActivity;

import java.io.File;

public class AlertBuilder {

    public static void showCloseApp(GoogleMapsActivity mapsActivity) {
        String yes = App.context().getString(R.string.global_yes);
        String no = App.context().getString(R.string.global_no);
        String title = App.context().getString(R.string.alert_close_app_title).toUpperCase();
        new AlertDialog.Builder(mapsActivity)
                .setCancelable(true)
                .setTitle(title)
                .setNegativeButton(yes.toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mapsActivity.finishAndRemoveTask();
                    }
                })
                .setPositiveButton(no.toUpperCase(), null)
                .setIcon(R.drawable.icon_alert_question)
                .show();
    }

    public static void showDeletePicture(Card card, String pictureFilePath, ReportActivity activity) {
        String yes = App.context().getString(R.string.global_yes);
        String no = App.context().getString(R.string.global_no);
        String title = App.context().getString(R.string.alert_delete_picture).toUpperCase();
        new AlertDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(title)
                .setNegativeButton(yes.toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LocalFileStorageService.deleteRecursive(new File(pictureFilePath));
                        card.setDismissible(true);
                        card.dismiss();
                    }
                })
                .setPositiveButton(no.toUpperCase(), null)
                .setIcon(R.drawable.icon_alert_question)
                .show();
    }

    public static void showDeleteReport(Card card, ReportOverviewActivity activity) {
        String yes = App.context().getString(R.string.global_yes);
        String no = App.context().getString(R.string.global_no);
        String title = App.context().getString(R.string.alert_delete_report).toUpperCase();
        new AlertDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(title)
                .setNegativeButton(yes.toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Report report = (Report) card.getTag();
                        LocalFileStorageService.deleteReport(report);
                        card.setDismissible(true);
                        card.dismiss();
                    }
                })
                .setPositiveButton(no.toUpperCase(), null)
                .setIcon(R.drawable.icon_alert_question)
                .show();
    }

    public static void showDiscardChanges(ReportActivity reportActivity) {
        String discard = App.context().getString(R.string.global_discard);
        String save = App.context().getString(R.string.global_save);
        String title = App.context().getString(R.string.alert_discard_changes).toUpperCase();
        new AlertDialog.Builder(reportActivity)
                .setCancelable(true)
                .setTitle(title)
                .setNeutralButton(discard.toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Report.reset();
                        reportActivity.finish();
                    }
                })
                .setPositiveButton(save.toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        reportActivity.saveReport();
                        Report.reset();
                        reportActivity.finish();
                    }
                })
                .setIcon(R.drawable.icon_alert_question)
                .show();
    }

    public static void showInputValidation(String message) {
        String ok = App.context().getString(R.string.global_ok);
        new AlertDialog.Builder(App.getReportActivity())
                .setCancelable(true)
                .setMessage(message)
                .setPositiveButton(ok.toUpperCase(), null)
                .setIcon(R.drawable.icon_alert_error)
                .show();
    }

    public static void showLocationAccessNotEnabled(GoogleMapsActivity mapsActivity) {
        String ok = App.context().getString(R.string.global_ok);
        String title = App.context().getString(R.string.alert_location_access_title).toUpperCase();
        String message = String.format(App.context().getString(R.string.alert_location_access_message));
        new AlertDialog.Builder(mapsActivity)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(ok.toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConfigurationProxy.requestLocationSettings(mapsActivity);
                    }
                })
                .setIcon(R.drawable.icon_alert_error)
                .show();
    }

    public static void showLocationPermissionNotEnabled(GoogleMapsActivity mapsActivity) {
        String ok = App.context().getString(R.string.global_ok);
        String title = App.context().getString(R.string.alert_location_permission_title).toUpperCase();
        String message = String.format(App.context().getString(R.string.alert_location_permission_message));
        new AlertDialog.Builder(mapsActivity)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(ok.toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConfigurationProxy.requestLocationPermissions(mapsActivity);
                    }
                })
                .setIcon(R.drawable.icon_alert_error)
                .show();
    }

    public static void showLocationRequired(GoogleMapsActivity mapsActivity) {
        String gotIt = App.context().getString(R.string.global_got_it);
        String update = App.context().getString(R.string.global_update);
        String title = App.context().getString(R.string.alert_location_required_title).toUpperCase();
        String message = App.context().getString(R.string.alert_location_required_message);

        new AlertDialog.Builder(mapsActivity)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(update.toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mapsActivity.isRequirementsGiven(true))
                            mapsActivity.updateCurrentLocation();
                    }
                })
                .setPositiveButton(gotIt.toUpperCase(), null)
                .setIcon(R.drawable.icon_alert_error)
                .show();
    }

    public static void showReasonWhyAccountIsNeeded(OnboardingActivity onboardingActivity) {
        String ok = App.context().getString(R.string.global_ok);
        String title = App.context().getString(R.string.alert_link_account_text).toUpperCase();
        String message = App.context().getString(R.string.alert_link_account_message);
        new AlertDialog.Builder(onboardingActivity)
                .setCancelable(true)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(ok.toUpperCase(), null)
                .setIcon(R.drawable.icon_alert_question)
                .show();
    }

    public static void showShareHint(ReportActivity reportActivity) {
        String cancel = App.context().getString(R.string.global_cancel);
        String continu = App.context().getString(R.string.global_continue);
        String title = App.context().getString(R.string.alert_share_hint_email_title).toUpperCase();
        AlertDialog dialog = new AlertDialog.Builder(reportActivity)
                .setCancelable(false)
                .setView(R.layout.alert_share_hint_email)
                .setTitle(title)
                .setPositiveButton(continu.toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        reportActivity.saveAndShareReport();
                    }
                })
                .setNeutralButton(cancel.toUpperCase(), null)
                .setIcon(R.drawable.icon_info)
                .show();
        CheckBox checkBoxGotIt = dialog.findViewById(R.id.checkBoxGotIt);
        checkBoxGotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox checkBox = (CheckBox) v;
                boolean isChecked = checkBox.isChecked();
                new SharedPreferencesEditor().setDoShareHint(isChecked);
            }
        });
    }
}
