package org.rotary.iyfr.plasticfreewaters.entity;

import com.google.gson.Gson;

import org.rotary.iyfr.plasticfreewaters.service.SharedPreferencesEditor;
import org.rotary.iyfr.plasticfreewaters.service.LocalFileStorageService;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class Report implements Serializable {

    private long id; // id is time in ms
    private Waypoint waypoint;
    private String name;
    private String category = "";
    private String dimension = "";
    private String depth = "";
    private String description = "";
    private ArrayList<String> pictures = new ArrayList<String>();
    private String notifier;
    private boolean isSaved = false;
    private boolean isSend = false;

    public Report(Waypoint waypoint) {
        setId(System.currentTimeMillis());
        setWaypoint(waypoint);
        setNotifier(SharedPreferencesEditor.getNotifier());
    }

    public static void reset() {
        SharedPreferencesEditor.saveReport(null);
    }

    public String getCategory() {
        return category;
    }

    public String getDepth() {
        return depth;
    }

    public String getDescription() {
        return description;
    }

    public String getDimension() {
        return dimension;
    }

    public long getId() {
        return id;
    }

    public String getLocalDateAndTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(id);
        calendar.setTimeZone(TimeZone.getDefault());
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy (HH:mm)");
        return sdf.format(calendar.getTime());
    }

    public String getName() {
        return name;
    }

    public String getNameForGpx() {
        if (name == null || name.trim().isEmpty() || name.toLowerCase().equals("null"))
            return getId() + LocalFileStorageService.FILE_EXTENSION_GPX;
        return name;
    }

    public String getNameForUi(String divider) {
        if (name == null || name.trim().isEmpty() || name.toLowerCase().equals("null"))
            return Waypoint.getLatLngFormattedNorthEast(getWaypoint().getLatitude(), getWaypoint().getLongitude(), divider);
        return name;
    }

    public String getNotifier() {
        return notifier;
    }

    public ArrayList<String> getPictures() {
        return pictures;
    }

    public long getReportTime() {
        return id;
    }

    public Waypoint getWaypoint() {
        return waypoint;
    }

    public boolean hasPictures() {
        return pictures.size() > 0;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public boolean isSend() {
        return isSend;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNotifier(String notifier) {
        this.notifier = notifier;
    }

    public void setPictures(ArrayList<String> pictures) {
        this.pictures = pictures;
    }

    public void setSaved(boolean trueFalse) {
        isSaved = trueFalse;
    }

    public void setSend(boolean send) {
        isSend = send;
    }

    public void setWaypoint(Waypoint waypoint) {
        this.waypoint = waypoint;
    }

    public void toCache() {
        SharedPreferencesEditor.saveReport(this);
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this, Report.class);
    }
}
