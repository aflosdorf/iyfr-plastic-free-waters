package org.rotary.iyfr.plasticfreewaters.ui;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.service.SharedPreferencesEditor;
import org.rotary.iyfr.plasticfreewaters.service.ConfigurationProxy;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConfigurationProxy.REQUESTCODE_CHOOSE_ACCOUNT) {
            if (resultCode == Activity.RESULT_OK) {
                /* Find out and save the email address or identity */
                String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                SharedPreferencesEditor.saveUserIdentity(accountName);
                TextInputEditText textFieldLinkedAccount = findViewById(R.id.textFieldLinkedAccount);
                textFieldLinkedAccount.setText(accountName.trim());
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initView();
    }

    private void initView() {
        /* Action Bar */
        getSupportActionBar().setTitle(getString(R.string.menu_item_settings));
        /* Name */
        TextInputEditText textFieldName = findViewById(R.id.textFieldName);
        textFieldName.setText(SharedPreferencesEditor.getUserName());
        textFieldName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35), ConfigurationProxy.symbolFilter});
        textFieldName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {/**/ }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {/**/ }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                TextInputLayout textInputLayoutName = findViewById(R.id.textInputLayoutName);
                if (s.toString().trim().isEmpty()) {
                    textInputLayoutName.setErrorEnabled(true);
                    textInputLayoutName.setError(getString(R.string.onboarding_enter_name_helper_text));
                } else {
                    textInputLayoutName.setErrorEnabled(false);
                    textInputLayoutName.setError(null);
                    SharedPreferencesEditor.saveUserName(s.toString());
                }
            }
        });

        /* Linked account */
        TextInputEditText textFieldLinkedAccount = findViewById(R.id.textFieldLinkedAccount);
        textFieldLinkedAccount.setText(SharedPreferencesEditor.getUserIdentity());

        /* Configured authority */
        TextInputEditText textFieldConfiguredAuthority = findViewById(R.id.textFieldConfiguredAuthority);
        textFieldConfiguredAuthority.setText(ConfigurationProxy.getEMailAdressOfAuthority());
    }

    public void onClickFabReselectAccount(View view) {
        Intent intent = AccountManager.newChooseAccountIntent(null, null, new String[]{"com.google", "com.google.android.legacyimap"}, null, null, null, null);
        startActivityForResult(intent, ConfigurationProxy.REQUESTCODE_CHOOSE_ACCOUNT);
    }
}