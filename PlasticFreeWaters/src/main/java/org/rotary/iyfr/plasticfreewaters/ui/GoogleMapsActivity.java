package org.rotary.iyfr.plasticfreewaters.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.CancellationToken;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnTokenCanceledListener;
import com.google.android.gms.tasks.Task;
import com.nambimobile.widgets.efab.Overlay;

import org.rotary.iyfr.plasticfreewaters.R;
import org.rotary.iyfr.plasticfreewaters.service.AlertBuilder;
import org.rotary.iyfr.plasticfreewaters.service.App;
import org.rotary.iyfr.plasticfreewaters.service.ApplicationMonitor;
import org.rotary.iyfr.plasticfreewaters.service.ConfigurationProxy;
import org.rotary.iyfr.plasticfreewaters.service.LocalFileStorageService;

public class GoogleMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final float ZOOM_OVERVIEW = 10;
    private static final float ZOOM_DETAIL = 14;
    private static final int ZOOM_DURATION_100 = 100;
    private static final int ZOOM_DURATION_250 = 250;

    private GoogleMap mMap;
    private Location currentLocation;
    private FusedLocationProviderClient fusedLocationClient;
    private ApplicationMonitor applicationMonitor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        applicationMonitor = new ApplicationMonitor();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        isRequirementsGiven(true);
        String testString = ConfigurationProxy.getTestString();
        App.setGoogleMapsActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Overlay fabmOverlay = findViewById(R.id.fabmOverlay);
        fabmOverlay.callOnClick();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((currentLocation == null) ||
                (!currentLocation.getProvider().contains("self") && System.currentTimeMillis() - currentLocation.getTime() > 5 * 60 * 1000)) {
            if (mMap != null && isRequirementsGiven(false))
                updateCurrentLocation();
        }
    }

    /**
     * Konvertiert ein {@link Drawable} zu einem {@link BitmapDescriptor}, um diesen
     * als Marker auf der Karte anzuzeigen.
     */
    protected BitmapDescriptor vectorToBitmapDescriptor(@DrawableRes int id, @ColorInt Integer color) {
        Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), id, null);
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        if (color != null)
            DrawableCompat.setTint(vectorDrawable, color);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void positionMarkerAtNewLocation(Location location, float zoom) {
        if (location != null) {
            currentLocation = location;
            LatLng floatingItem = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            mMap.clear();
            String title = getString(R.string.your_position);
            if (location.getProvider().contains("self"))
                title = getString(R.string.pinned_position);
            Marker positionOfWaste = mMap.addMarker(new MarkerOptions()
                    .position(floatingItem)
                    .title(title)
                    .icon(vectorToBitmapDescriptor(R.drawable.position_marker_blue, null)));
            positionOfWaste.showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(floatingItem, zoom), ZOOM_DURATION_100, null);
        }
    }

    public boolean isRequirementsGiven(boolean doAlert) {
        boolean isLocationPermissionEnabled = ConfigurationProxy.isLocationPermissionEnabled();
        boolean isLocationAccessEnabled = ConfigurationProxy.isLocationAccessEnabled();
        if (!isLocationPermissionEnabled && doAlert) {
            AlertBuilder.showLocationPermissionNotEnabled(this);
        }
        if (!isLocationAccessEnabled && doAlert) {
            AlertBuilder.showLocationAccessNotEnabled(this);
        }
        return isLocationPermissionEnabled && isLocationAccessEnabled;
    }

    @Override
    public void onBackPressed() {
        Overlay fabmOverlay = findViewById(R.id.fabmOverlay);
        fabmOverlay.callOnClick();
        AlertBuilder.showCloseApp(this);
    }

    public void onClickFabAboutUs(View view) {
        /* Open AboutUsActivity */
        Intent aboutUsActivity = new Intent(this, AboutUsActivity.class);
        startActivity(aboutUsActivity);
    }

    public void onClickFabAddNew(View view) {
        if (currentLocation == null) {
            /* Info to the user */
            AlertBuilder.showLocationRequired(this);
        } else {
            /* New report */
            Intent newReportIntent = new Intent(this, ReportActivity.class);
            newReportIntent.putExtra(ReportActivity.INTENT_EXTRA_LOCATION, currentLocation);
            newReportIntent.putExtra(ReportActivity.INTENT_EXTRA_REPORT_ID, 0);
            startActivity(newReportIntent);
        }
    }

    public void onClickFabShowReportOverview(View view) {
        /* Report Overview */
        Intent newReportOverviewIntent = new Intent(this, ReportOverviewActivity.class);
        startActivity(newReportOverviewIntent);
    }

    public void onClickSettings(View view) {
        /* Open SettingsActivity */
        Intent settingsActivity = new Intent(this, SettingsActivity.class);
        startActivity(settingsActivity);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng point) {
                Location location = new Location("self");
                location.setLatitude(point.latitude);
                location.setLongitude(point.longitude);
                location.setTime(System.currentTimeMillis());
                positionMarkerAtNewLocation(location, ZOOM_OVERVIEW);
            }
        });
        /* Bis hier ging es noch ohne Permission, weitere Funktionen der Map jedoch nicht! */
        if (isRequirementsGiven(false)) {
            mMap.setMyLocationEnabled(true);
            mMap.setIndoorEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    updateCurrentLocation();
                    return false;
                }
            });
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    positionMarkerAtNewLocation(location, ZOOM_OVERVIEW);
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ConfigurationProxy.REQUESTCODE_LOCATION_PERMISSIONS) {
            /* Initialize the map again. */
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    public void takeScreenshot(long recordingId) {
        positionMarkerAtNewLocation(currentLocation, ZOOM_OVERVIEW);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                                  @Override
                                  public void onSnapshotReady(@Nullable Bitmap bitmap) {
                                      LocalFileStorageService.saveScreenshot(recordingId, bitmap);
                                      App.getReportActivity().updatePictureScollView();
                                  }
                              }
                );
            }
        }, ZOOM_DURATION_250);
    }

    @SuppressLint("MissingPermission")
    public void updateCurrentLocation() {
        Task<Location> currentLocation = fusedLocationClient.getCurrentLocation(LocationRequest.PRIORITY_HIGH_ACCURACY, new CancellationToken() {
            @Override
            public boolean isCancellationRequested() {
                return false;
            }

            @NonNull
            @Override
            public CancellationToken onCanceledRequested(@NonNull OnTokenCanceledListener onTokenCanceledListener) {
                return null;
            }
        });
        currentLocation.addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Location result = task.getResult();
                positionMarkerAtNewLocation(result, ZOOM_OVERVIEW);
            }
        });
    }
}